"use strict";
module.exports = function(app) {
    const phones = require("../controllers/phonesController");

    // phones Routes
    app.route("/phones").get(phones.list_all_phones);
};
