"use strict";

const phones = require("../data/phones.json");

exports.list_all_phones = function(req, res) {
    setTimeout(function() {
        res.json(phones);
    }, 2000);
};
