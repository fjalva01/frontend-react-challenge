import React from "react";
import "./App.css";
import { Route, BrowserRouter as Router } from "react-router-dom";
import PhoneListContainer from "./components/PhoneListContainer";
import PhoneDetailComponent from "./components/PhoneDetailComponent";

const routing = (
    <Router>
        <div className="app__content">
            <Route exact path="/phones" component={PhoneListContainer} />
            <Route path="/phones/:id" component={PhoneDetailComponent} />
        </div>
    </Router>
);

function App() {
    return (
        <div className="app__container">
            {/* <Header /> */}
            {routing}
        </div>
    );
}

export default App;
