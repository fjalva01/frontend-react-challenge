import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "../Loader";
import { fetchPhones } from "../../redux/actions";
import Header from "../Header";

class PhoneDetailComponent extends Component {
    componentDidMount() {
        this.props.dispatch(fetchPhones());
    }

    render() {
        const { loading, phone } = this.props;
        return loading ? (
            <div className="phone-detail__full-size-center">
                <Loader />
            </div>
        ) : phone ? (
            <div className="phone-detail__container">
                <Header>
                    <Link to="/phones">{`<- phones`}</Link>
                </Header>
                <div className="phone-detail__content">
                    <div className="phone-detail__image">
                        <img
                            src={phone.image}
                            alt={phone.vendor + " " + phone.model}
                            className="phone-detail__image"
                        ></img>
                    </div>
                    <div className="phone-detail__side">
                        <span>{phone.vendor}</span>
                        <span>{phone.model}</span>
                        <span>{phone.color}</span>
                        <span>{`${phone.price}€`}</span>
                    </div>
                </div>
            </div>
        ) : (
            <div></div>
        );
    }
}

const mapStateToProps = (state, params) => ({
    phone: state.itemsById[params.match.params.id],
    id: params.match.params.id,
    loading: state.loading
});

export default connect(mapStateToProps)(PhoneDetailComponent);
