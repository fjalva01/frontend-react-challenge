import React, { Component } from "react";
import "./index.css";
import Loader from "../Loader";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchPhones } from "../../redux/actions";
import Header from "../Header";

class PhoneListContainer extends Component {
    state = {
        pageSelected: 1
    };

    perPage = 4;

    componentDidMount() {
        this.props.dispatch(fetchPhones());
    }

    onPageChange = pageSelected => {
        this.setState({ pageSelected });
    };

    getPhoneRange = phones =>
        phones.slice(
            (this.state.pageSelected - 1) * this.perPage,
            this.state.pageSelected * this.perPage
        );

    render() {
        const { loading, phones } = this.props;
        const { pageSelected } = this.state;
        let pages = [];
        if (phones && phones.length) {
            pages = Array(Math.ceil(phones.length / this.perPage))
                .fill(1)
                .map((x, y) => x + y);
        }
        const phonesRange = this.getPhoneRange(phones);
        return loading ? (
            <div className="phone-list-container__full-size-center">
                <Loader />
            </div>
        ) : (
            <div className="phone-list-container__container">
                <Header>
                    <span className="phone-list-container__title">
                        Etereo phone shop
                    </span>
                </Header>
                <div className="phone-list-container__content">
                    {phonesRange.map(phone => (
                        <Link key={phone._id} to={`/phones/${phone._id}`}>
                            <div className="phone-list-container__item">
                                <img
                                    src={phone.image}
                                    alt={phone.vendor + " " + phone.model}
                                    className="phone-list-container__image"
                                ></img>
                                <span>{phone.vendor}</span>
                                <span>{phone.model}</span>
                            </div>
                        </Link>
                    ))}
                </div>
                <div className="paginator">
                    {pages.map((page, i) => (
                        <button
                            key={`page__` + page}
                            className={pageSelected === i + 1 ? "selected" : ""}
                            onClick={() => this.onPageChange(page)}
                        >
                            {page}
                        </button>
                    ))}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    phones: state.items,
    loading: state.loading
});

export default connect(mapStateToProps)(PhoneListContainer);
